# https://github.com/strapi/strapi-docker#how-to-use-strapibase

FROM strapi/base

#WORKDIR /my-path

COPY app/package.json ./
COPY app/yarn.lock ./

RUN yarn install

COPY app .

ENV NODE_ENV production

RUN yarn build

EXPOSE 1337

CMD ["yarn", "start"]
